[![Project](https://badgen.net/badge/project/Cppcheck/orange?icon=gitlab)](https://gitlab.com/bi_zeps/ca_cppcheck/-/blob/main/README.md#cppcheck)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%2Fissues_statistics)](https://gitlab.com/bi_zeps/ca_cppcheck/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%2Frepository%2Fcommits%3Fbranch%3Dmain)](https://gitlab.com/bi_zeps/ca_cppcheck/-/commits/main)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%3Flicense%3Dtrue)](https://gitlab.com/bi_zeps/ca_cppcheck/-/blob/main/LICENSE) applies to software of this project. The running software within the image/container ships with its own license(s).

[![Stable Version](https://img.shields.io/docker/v/bizeps/cppcheck/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/ca_cppcheck/-/blob/main/CHANGELOG.md#cppcheck)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/ca_cppcheck?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/cppcheck)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/ca_cppcheck/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/cppcheck)

# Cppcheck
Image with [Cppcheck](https://cppcheck.sourceforge.io/) installed.

`docker run --rm bizeps/cppcheck:latest cppcheck`, uses working directory `/var/build`.